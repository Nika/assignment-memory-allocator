#define _DEFAULT_SOURCE

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

// Взято из mem.c для тестирования
static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}
static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}

static uint8_t block_count_not_free(struct block_header* block) {
  uint8_t numb = 0;
  while (block) {
    if (!block->is_free) numb++;
    block = block->next;
  }

  return numb;
}


//Обычное успешное выделение памяти
static void test1(struct block_header* block) {
  puts("1) Обычное успешное выделение памяти:");
  void* test = _malloc(100);

  if (!test) {
    err("ERROR: Тест 1 - Функция _malloc вернула NULL!\n");
  }
  if (block->capacity.bytes != 100) {
    _free(test);
  	err("ERROR: Тест 1 - Размер блока неверный!\n");
  }
  struct block_header* block_header = block_get_header(test);
  if (block_header->is_free) {
    _free(test);
  	err("ERROR: Тест 1 - Блок помечен как незанятый!\n");
  }

  puts("Тест 1 пройден");
  _free(test);
}

//Освобождение одного блока из нескольких выделенных.
  static void test2(struct block_header* block) {
  puts("2) Освобождение одного блока из нескольких выделенных:");
  void* test1 = _malloc(100);
  void* test2 = _malloc(150);
  void* test3 = _malloc(200);
  if (!test1 || !test2 || !test3) {
  	err("ERROR: Тест 2 - Функция _malloc вернула NULL!\n");
  }
  if (block_count_not_free(block) != 3) {
    _free(test1);
    _free(test2);
    _free(test3);
  	err("ERROR: Тест 2 - Выделено неверное количество блоков!\n");
  }
  _free(test2);
  if(block_count_not_free(block) != 2) {
    _free(test1);
    _free(test3);
  	err("ERROR: Тест 2 - После освобождения количество блоков неверное!\n");
  }
  puts("Тест 2 пройден");
  _free(test1);
  _free(test3);
}


//Освобождение двух блоков из нескольких выделенных
static void test3(struct block_header* block) {
  puts("3) Освобождение двух блоков из нескольких выделенных:");
  void* test1 = _malloc(100);
  void* test2 = _malloc(150);
  void* test3 = _malloc(200);
  void* test4 = _malloc(250);
  if (!test1 || !test2 || !test3 || !test4) {
  	err("ERROR: Тест 3 - Функция _malloc вернула NULL!\n");
  }
  if (block_count_not_free(block) != 4) {
    _free(test1);
    _free(test2);
    _free(test3);
    _free(test4);
  	err("ERROR: Тест 3 - Выделено неверное количество блоков!\n");
  }
  _free(test2);
  _free(test3);
  if(block_count_not_free(block) != 2) {
    _free(test1);
    _free(test4);
  	err("ERROR: Тест 3 - После освобождения количество блоков неверное!\n");
  }

  puts("Тест 3 пройден");
  _free(test1);
  _free(test4);
}


//Память закончилась, новый регион памяти расширяет старый
static void test4(struct block_header* block) {
  puts("4) Память закончилась, новый регион памяти расширяет старый:");
  void* test1 = _malloc(1000);
  void* test2 = _malloc(1000);
  
  if(!test1 || !test2){
    err("ERROR: Тест 4 - Функция _malloc вернула NULL!\n");
  }
   if(block_count_not_free(block) != 2) {
    err("ERROR: Тест 4 - Kоличество блоков неверное!\n");
    _free(test1);
    _free(test2);
  }
  struct block_header* header1 = block_get_header(test1);
  struct block_header* header2 = block_get_header(test2);
  if ((uint8_t*)test1 + header1->capacity.bytes != (uint8_t*)header2) {
    _free(test1);
    _free(test2);
    err("ERROR: Тест 4 - Расположение блоков неверное!\n");
  }

  puts("Тест 4 пройден");
  _free(test1);
  _free(test2);
}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте
static void test5(struct block_header* block) {
    puts("5) Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте:");
    void* test1 = _malloc(1000);

    if(!test1){
      err("ERROR: Тест 5 - Функция _malloc вернула NULL!\n");
    }
    struct block_header* start_of_middle_region = block_after(block);
    map_pages(start_of_middle_region, 1000, MAP_FIXED_NOREPLACE);

    void* test2 = _malloc(10000);
    if(!test2){
      err("ERROR: Тест 5 - Функция _malloc вернула NULL!\n");
    }
    struct block_header* header2 = block_get_header(test2);
  
    if(start_of_middle_region == header2) {
      _free(test2);
      _free(test1);
      err("ERROR: Тест 5 - новый регион выделился сразу после старого!\n");
    }   
    puts("Тест 5 пройден");
    _free(test1);
    _free(test2);
}

static void start_tests(){
  void* heap = heap_init(1000);
  if (!heap) {
    err("Куча не создана\n");
  }
  else puts("Куча создана, выполнение тестов началось...");
  struct block_header* block = (struct block_header*) heap;
  test1(block);
  test2(block);
  test3(block);
  test4(block);
  test5(block);
  puts("Ура, все тесты пройдены!");
}

int main() {
  start_tests();
  return 0;
}
